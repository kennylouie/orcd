# Daemon

Executes commands on a unix environment.

# Usage

Check the status of the daemon server.
```
GET /check

response:
{
	"status": "online"
}
```


Start a workflow
```
POST /workflow

request:
{
	"ops": [
		{
                	"name": "@kennylouie/op1",
			"version": "0.1.6"
		},
		{
                	"name": "@kennylouie/op2",
			"version": "1.3.3"
		}
	]
}

response:
{
	"workflow_hash": "9hv29834484hfsd8fy348hisa"
}
```

Check status of running workflows
```
GET /workflows

response:
{
	active: [
		{
			"workflow_hash": "1824hr89hfd8gh34asdf22",
			"current_op": "op1"
		},
		{
			"workflow_hash": "289v92090g2h9IEDrn2Iopn34",
			"current_op": "op9"
		}
	]
}
```

Check details about a workflow
```
GET /workflow?hash={HASH}

response:
{
	"workflow_hash": "1298nv8924s98dfh82",
	"status": "failed",
	"ops": [
		{
                	"name": "@kennylouie/op1",
			"version": "0.1.6"
		},
		{
                	"name": "@kennylouie/op2",
			"version": "1.3.3"
		}
	],
	"last_op_ran": "@kennylouie/op1"
}
```

Get logs from a workflow
```
GET /logs?workflow={HASH}

response:
{
	"workflow_hash": "28vbD230dhnn2ASf2",
	"logs": [
		{
			"timestamp": "2020-01-20T01:34:33.333",
			"log": "stdout message..."
		},
		{
			"timestamp": "2020-01-20T01:34:33.333",
			"log": "stdout message..."
		}
	]
}
```

# building from source

```
make
```
