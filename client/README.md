# Client

Lives inside of an op (@kennylouie/cicd).

Sends http requests to the daemon.

# Usage

All commands below are compatible being run from an op.
```
ops run cicd [OPTIONS]
```

Terraform a remote linux server to be able to run ops.
```
./client terraform
```

will prompt for a team secrets variable corresponding to the remote server's IP address and private key.


Runs a workflow
```
./client create
```

will prompt to create a workflow to be run. Individual ops need to be created first.

Checks the status of a workflow
```
./client status
```

will prompt user to select from a currently running workflow.
