# orcd

The orc demon that runs ops.

Orcd is the orchestrator that deploys op workflows. It is a long lived daemon process that executes workflows based on a RESTful api via http json requests.

Consists:

+ daemon (C)
+ client (Go)

Depends on:

+ @cto.ai/ops
+ vault setup on your ops team
